<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('posts')->truncate();
        DB::table('posts')->insert([
            [
                'id' => '1',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '1',
                'thumbnail' =>'https://i.pinimg.com/236x/5e/93/d7/5e93d7b3b8f89c6e4e1884e06ca56a59.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ],
            [
                'id' => '2',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '2',
                'thumbnail' =>'https://i.pinimg.com/236x/6d/21/ca/6d21ca0c7bfed7abd55912c5d81a45c9.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ],
            [
                'id' => '3',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '3',
                'thumbnail' =>'https://i.pinimg.com/236x/cf/f3/55/cff355b18e1cb1ca04d619023300fa61.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ],
            [
                'id' => '4',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '1',
                'thumbnail' =>'https://i.pinimg.com/236x/28/c9/37/28c937cdf6b624074bd34c9d105c83f8.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ],
            [
                'id' => '5',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '2',
                'thumbnail' =>'https://i.pinimg.com/236x/e3/6b/9b/e36b9b3f2cd93b3787894d0c97ec101e.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ],
            [
                'id' => '6',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '3',
                'thumbnail' =>'https://i.pinimg.com/236x/9d/16/64/9d16649e2bb4064b2c492836e62d2575.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ],       [
                'id' => '7',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '1',
                'thumbnail' =>'https://i.pinimg.com/236x/9d/16/64/9d16649e2bb4064b2c492836e62d2575.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ],
            [
                'id' => '8',
                'title' => 'Title Definition & Meaning - Merriam-Webster',
                'discription' => 'A paragraph is a series of related sentences developing a central idea, called the topic. Try to think about paragraphs in terms of thematic unity: a paragraph is a sentence or a group of sentences that supports one central, unified idea. Paragraphs add one idea at a time to your broader argument.',
                'content'=> '',
                'category_id'=> '1',
                'thumbnail' =>'https://i.pinimg.com/236x/9d/16/64/9d16649e2bb4064b2c492836e62d2575.jpg',
                'created_at' => '2000-01-01',
                'updated_at' => '2000-01-01',
                'user_id' => 1,
            ]


        ]);
       DB::statement('SET FOREIGN_KEY_CHECKS = 1');
}
}
