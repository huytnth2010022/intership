<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('users')->truncate();
        DB::table('users')->insert([  
            [
                'id' => '1',
                'first_name' => 'John',
                'last_name'=> 'Wich',
                'user_name'=> 'TestDemo',
                'thumbnail' => "https://i.pinimg.com/236x/70/0e/2d/700e2d692a9ad445874ab23578ef535a.jpg",
                'email' =>'demo@gmail.com',
                'password' => bcrypt('huy123'),
                'birth_day' => Carbon::now(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ], 
           
        ]);
       DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
