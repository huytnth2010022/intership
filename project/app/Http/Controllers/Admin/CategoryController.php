<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Repository\CategoryRebository;
use App\Repository\PostRebository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryRebository;
    public function __construct(Request $request)
    {
        $this->data = $request->all();
        $this->categoryRebository = new CategoryRebository();
    }
    public function updateCategoryView()
    {
        $categories = $this->categoryRebository->getTreeArray();
        return view("CreateAdmin.create_category",["categories"=>$categories]);
    }

    public function updateCategoryPost()
    {
        $result = $this->categoryRebository->updateCategory($this->data["data"]);
        return view("List.list-parent-category",["categories"=>$result]);
    }
    public function deleteCategory(){
        $category = Category::find($this->data["category_id"])->delete();
        $categories = $this->categoryRebository->getTreeArray();
        return view("List.list-parent-category",["categories"=>$categories]);
    }
    public function createCategory(){
        $result = $this->categoryRebository->createCategory($this->data);
        $categories = $this->categoryRebository->getTreeArray();
        return view("List.list-parent-category",["categories"=>$categories]);
    }
}
