<?php

namespace App\Repository;

use App\Models\Post;
use Carbon\Carbon;

class PostRebository
{
    public function createPost($data)
    {
        $post = new Post();
        isset($data["title"]) ? $post->title = $data["title"] : $post->title = "";
        isset($data["category_id"]) ? $post->category_id = $data["category_id"] : $post->category_id = "";
        isset($data["discription"]) ? $post->discription = $data["discription"] : $post->discription = "";
        isset($data["user_id"]) ? $post->user_id = $data["user_id"] : $post->user_id = "";
        isset($data["created_at"]) ? $post->created_at = $data["created_at"] : $post->created_at = Carbon::now();
        isset($data["content"]) ? $post->content = $data["content"] : $post->content = "";
        isset($data["image"]) ? $post->thumbnail = $this->UploadImage($data) : $post->thumbnail = Post::$default_thumbnail_url;
        $post->save();
        return $post;
    }
    public function updatePost($data)
    {
        if (isset($data["id"])){
            $post = Post::find($data["id"]);
            isset($data["title"]) ? $post->title = $data["title"] : $post->title = "";
            isset($data["category_id"]) ? $post->category_id = $data["category_id"] : $post->category_id = "";
            isset($data["discription"]) ? $post->discription = $data["discription"] : $post->discription = "";
            isset($data["user_id"]) ? $post->user_id = $data["user_id"] : $post->user_id = "1";
            isset($data["created_at"]) ? $post->created_at = $data["created_at"] : $post->created_at = Carbon::now();
            isset($data["content"]) ? $post->content = $data["content"] : $post->content = "";
            if(isset($data["image"])){
                $post->thumbnail = $this->UploadImage($data);
            }
            $post->update();
        }else{
            $post = new Post();
        }
        return $post;
    }
    public function UploadImage($data)
    {
        $input['image'] = time() . '.' . $data["image"]->extension();
        $data["image"]->move(public_path('user.img'), $input['image']);
        return '\user.img\\' . $input['image'];
    }
}
