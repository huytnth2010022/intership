<?php

namespace App\Repository;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AccountRebository
{
    public function createAccount($data)
    {
$user = $this->getUser($data);
$dataInsert =[
    'user_name'=>$user->user_name,
    'password'=>$user->password,
    'first_name'=>$user->first_name,
    'last_name'=>$user->last_name,
    'email'=>$user->email,
    'birth_day'=>$user->birth_day,
    'thumbnail'=>$user->thumbnail,
    'created_at'=>$user->created_at,
    'updated_at'=>$user->updated_at,
];
$user = User::create($dataInsert);
        if($user!=null){
            DB::insert('insert into roles_user (role_id, user_id) values (?, ?)', [1, $user->id]);
        }
        return $user;
    }

public function getUser($data){
    $user = new User();
    isset($data["password"]) ? $user->setPasswordAttribute($data["password"]) : $user->password = "";
    if (isset($data["birthday"])) {
        $date = strtotime($data["birthday"]);
        $user->birth_day = date('Y-m-d H:i:s', $date);
    } else {
        $user->birth_day = "";
    }
    isset($data["first_name"]) ? $user->first_name = $data["first_name"] : $user->first_name = "";
    isset($data["last_name"]) ? $user->last_name = $data["last_name"] : $user->last_name = "";
    isset($data["email"]) ? $user->email = $data["email"] : $user->email = "";
    isset($data["user_name"]) ? $user->user_name = $data["user_name"] : $user->user_name = "";

    isset($data["image"]) ? $user->thumbnail = $this->UploadImage($data) : $user->thumbnail = User::$default_thumbnail_url;
    $user->created_at = Carbon::now();
    $user->updated_at = Carbon::now();
    return $user;
}

    public function UploadImage($data)
    {
        $input['image'] = time() . '.' . $data["image"]->extension();
        $data["image"]->move(public_path('user.img'), $input['image']);
        return '\user.img\\' . $input['image'];
    }
}
